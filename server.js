const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./Backend/routes');


//Setup Express App
const app = express();

//to parse the body with post method
app.use(bodyParser.json());

//to handle CORS error
app.use(function (req, res, next) {

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

//to use the routes of express
app.use('/', routes);


//listen for requests
app.listen(process.env.port || 4242, () => {
    console.log('server is running!!')
});;
