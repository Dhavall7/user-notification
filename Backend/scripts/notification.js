const fs = require('fs');

module.exports = {
    async fetchNotification(req, res) {
        try {
            //to read data from files and send them in success response 
            fs.readFile('./Backend/uploads/invitations.json', 'utf8', function (err, data) {
                if (err) throw err;
                let invitations = JSON.parse(data);
                return res.json({ result: "success", notificationdata: invitations });
            });
        }
        catch (err) {
            return console.error(err);
        }
    },
    async fetchUpdatedNotification(req, res) {
        try {
            fs.readFile('./Backend/uploads/invitations_update.json', 'utf8', function (err, data) {
                if (err) throw err;
                let updatedInvitations = JSON.parse(data);
                return res.json({ result: "success", notificationdata: updatedInvitations });
            });
        }
        catch (err) {
            return console.error(err);
        }
    }
}