import React, { useState, useEffect } from 'react';
import axios from 'axios';
import '../Styles/notification.css'
import DoneIcon from '@material-ui/icons/Done';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import PersonIcon from '@material-ui/icons/Person';
import TwitterIcon from '@material-ui/icons/Twitter';
import EmailIcon from '@material-ui/icons/Email';

function Notification() {
    const [notification, setNotification] = useState([]);
    //Made api call on load of notification component 
    useEffect(() => {
        axios.get(`http://localhost:4242/`)
            .then(response => {
                if (response.status === 200) {
                    setNotification(response.data.notificationdata.invites);
                }
            })

        //below method is used to fetch updated data after specific fix amount of time
        setTimeout(() => {
            axios.get(`http://localhost:4242/updatedData`)
                .then(response => {
                    if (response.status === 200) {
                        setNotification(response.data.notificationdata.invites)
                    }

                })

        }, 5000);
    }, []);
    return (
        <div>
            <h1>Notification List</h1>
            <div className="notification_container">
                {
                    notification && notification.map((item, key) => {

                        //To get hour/minute data from timestamp
                        let hour = new Date(item.invite_time).getHours();
                        let minute = new Date(item.invite_time).getMinutes();

                        hour = (hour < 10) ? '0' + hour : hour;
                        minute = (minute < 10) ? '0' + minute : minute;

                        return <div >
                            <div key={key} className={item.status === "unread" ? "notification_container_unread" : "notification_container_read"}>
                                <div className="notification_sender">{item.sender_id}</div>
                                <div className="notification_vector">
                                    {item.vector === "Email" ? <EmailIcon style={{ color: "blue" }} /> : item.vector === "Twitter" ? <TwitterIcon style={{ color: "blue" }} /> : <PersonIcon style={{ color: "blue" }} />}
                                </div>
                                <div className="notification_invite" >{item.invite}</div>
                                <div className="notification_timestamp">{hour + ':' + minute}</div>
                                <div className="notification_view" >
                                    {item.status === "unread" ? <DoneIcon style={{ color: "blue" }} /> : <DoneAllIcon style={{ color: "blue" }} />}
                                </div>
                            </div>
                            {
                                notification && (notification.length - 1) === key ? null : <hr />
                            }

                        </div>

                    })
                }
            </div>
        </div>
    )
}

export default Notification
