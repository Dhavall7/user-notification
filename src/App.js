import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
//used react-router-dom for routing in react
import Notification from './Components/Notification';

//Used below routes to map it with specific components 
function App() {
  return (
    <Router>
        <div className="App">
          <Switch>
            <Route path="/">
              <Notification/>
            </Route>
          </Switch>
        </div>
    </Router>
  );
}

export default App;
